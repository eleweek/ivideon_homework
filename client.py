import curses
import struct
from tornado import gen
from tornado.ioloop import IOLoop
import tornado.tcpclient
import argparse
import sys
from contextlib import closing

CURSES_LAMP_COLOR_INDEX = 1
CURSES_LAMP_COLOR_PAIR = 1


# Potentially, it might makes sense to decouple drawing logic into a separate class
# But keeping it in a single class is simpler now
class Lamp(object):
    def __init__(self, window, ascii_art, is_on=True, r=255, g=0, b=0):
        self.is_on = is_on
        self.window = window
        self.ascii_art = ascii_art
        self.change_color(r, g, b)
        self._render()

    def turn_on(self):
        self.is_on = True
        self._render()

    def turn_off(self):
        self.is_on = False
        self._render()

    def change_color(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b
        self._update_curses_color_and_render()

    def _update_curses_color_and_render(self):
        remapped_colors_for_curses = map(lambda color: int(color / 255.0 * 1000.0), [self.r, self.g, self.b])

        self.window.clear()
        curses.init_color(CURSES_LAMP_COLOR_INDEX, *remapped_colors_for_curses)
        self._render()

    def _render(self):
        OFFSET_Y = 5
        OFFSET_X = 5
        self.window.clear()
        self.window.addstr(OFFSET_Y, OFFSET_X, "Lamp state: {}".format("on" if self.is_on else "off"))
        self.window.addstr(OFFSET_Y + 1, OFFSET_X, "Lamp color: ")
        self.window.addstr("({}, {}, {})".format(self.r, self.g, self.b), curses.color_pair(CURSES_LAMP_COLOR_PAIR))
        if self.is_on:
            for line_num, line in enumerate(ascii_art):
                self.window.addstr(OFFSET_Y + 3 + line_num, OFFSET_X, line, curses.color_pair(CURSES_LAMP_COLOR_INDEX))
        window.refresh()


@gen.coroutine
def run(host, port, lamp):
    client = tornado.tcpclient.TCPClient()
    supported_messages = {
        # Type: value fmt, handler
        0x12: ("", lamp.turn_on),
        0x13: ("", lamp.turn_off),
        0x20: (">BBB", lamp.change_color)
    }

    with closing(client):
        stream = yield client.connect(host, port)

        while True:
            try:
                msgtype_packed = yield stream.read_bytes(1)
            except tornado.iostream.StreamClosedError:
                return
            msgtype, = struct.unpack(">B", msgtype_packed)
            print >> sys.stderr, type(msgtype)

            msglen_packed = yield stream.read_bytes(2)
            msglen, = struct.unpack(">H", msglen_packed)

            msgvalue = yield stream.read_bytes(msglen)
            if msgtype not in supported_messages:
                continue
            value_fmt, msg_handler = supported_messages[msgtype]

            msg_handler_args = struct.unpack(value_fmt, msgvalue)
            msg_handler(*msg_handler_args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Simulate the lamp controlling protocol')
    parser.add_argument('--host', default="127.0.0.1", help='host to connect to')
    parser.add_argument('--port', default=9999, type=int, help='port to connect to')
    args = parser.parse_args()

    window = curses.initscr()
    try:
        ascii_art = [l.strip('\n') for l in open("lamp.txt", "r")]

        curses.start_color()
        curses.use_default_colors()
        curses.init_pair(CURSES_LAMP_COLOR_PAIR, CURSES_LAMP_COLOR_INDEX, -1)

        lamp = Lamp(window, ascii_art)

        IOLoop.instance().run_sync(lambda: run(args.host, args.port, lamp))
    finally:
        curses.endwin()
        print >> sys.stderr, "The program has finished successfully"
