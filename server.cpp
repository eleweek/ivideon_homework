#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/system/system_error.hpp>

using boost::asio::ip::tcp;
typedef boost::shared_ptr<tcp::socket> socket_ptr;

boost::mt19937 prng;

enum EEventType {
    ET_ON = 0,
    ET_OFF = 1,
    ET_COLOR = 2
};


/* Implementation details:
 * 1) We could reuse the same buffer instead of returning a new string every time.
 *    However returning std::string is simpler and performance doesn't seem to be an issue
 * 2) Using boost serialization seems to be an overkill here.
 *    reinterpret_cast + htonl isn't much better than the current code */
std::string generate_random_event_message()
{
    double event_probabilities[] = {
            0.2, 0.2, 0.6
    };
    boost::random::discrete_distribution<> event_distribution(event_probabilities);
    boost::random::uniform_int_distribution<> color_distribution(0, 255);

    std::string msg;

    EEventType et = (EEventType) event_distribution(prng);
    switch (et) {
        case ET_ON:
            msg.push_back(0x12);
            msg.append(2, 0);
            break;
        case ET_OFF:
            msg.push_back(0x13);
            msg.append(2, 0);
            break;
        case ET_COLOR:
            msg.push_back(0x20);
            msg.push_back(0);
            msg.push_back(3);
            for (int i = 0; i < 3; ++i) {
                msg.push_back(color_distribution(prng));
            }
            break;
        default:
            assert(false && "Generated invalid message type");
    }

    return msg;
}

void session(socket_ptr sock)
{
    sock->non_blocking(true);
    std::vector<char> read_buffer(4096);
    const int numEvents = 42;
    try {
        for (int i = 0; i < numEvents; ++i) {
            while (true) {
                boost::system::error_code ec;
                std::size_t bytes_transferred = boost::asio::read(*sock, boost::asio::buffer(read_buffer), ec);
                if (ec == boost::asio::error::eof)
                    return;

                if (bytes_transferred == 0 && ec == boost::asio::error::would_block)
                    break;
                if (ec)
                    throw boost::system::system_error(ec);
            }
            std::string msg = generate_random_event_message();
            boost::asio::write(*sock, boost::asio::buffer(msg));
            sleep(1);
        }
        sock->close();
    } catch (std::exception& e) {
        std::cerr << "Exception in thread: " << e.what() << "\n";
    }
}

void server(boost::asio::io_service& io_service, unsigned short port)
{
    tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
    while (true) {
        socket_ptr sock(new tcp::socket(io_service));
        a.accept(*sock);
        boost::thread t(boost::bind(session, sock));
    }
}


int main(int argc, char* argv[])
{
    try
    {
        unsigned int port;
        if (argc == 2) {
            port = boost::lexical_cast<unsigned short>(argv[1]);
        } else if (argc == 1) {
            port = 9999;
        } else {
          std::cerr << "Usage: server [<port>]\n";
          return 1;
        }

        boost::asio::io_service io_service;
        server(io_service, port);
    } catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
